# Philippe Charrière

- 🐳 Sr Solutions Architect @Docker
- 🦊🥑 Former Sr Customer Success Engineer @GitLab (+Sr CSM)
- 🦊 + 🐙 @k33g
- 🐦 @k33g_org
- 🍊🦸 Community Hero @gitpod
- ☸️ @CivoCloud Ambassador
- GDG Cloud IOT Lyon



 
